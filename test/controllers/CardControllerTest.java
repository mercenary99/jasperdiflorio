package controllers;

import models.Card;
import org.junit.Test;
import repository.CardRepository;
import org.mockito.Mockito;
import services.CardService;
import services.DefaultCardService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class CardControllerTest {


    @Test
    public void testCardController() {
        // Create and train mock repository
        CompletionStage<Card> future = CompletableFuture.supplyAsync(Card::new);
        CardRepository repositoryMock = Mockito.mock(CardRepository.class);
        Mockito.when(repositoryMock.find(1L)).thenReturn(future);
// Test Service
        Long cardid = 1L;
        CardService cardService = new DefaultCardService(repositoryMock);
        cardService.get(cardid);
// Verify that find method in book repository was called
        Mockito.verify(repositoryMock).find(cardid);
    }

}
