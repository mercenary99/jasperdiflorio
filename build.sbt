name := """jasperdiflorio"""
organization := "ch.fhgr"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.13.1"

libraryDependencies += guice

libraryDependencies ++= Seq(
  evolutions,
  javaJdbc,
  javaJpa,
  //"com.h2database" % "h2" % "1.4.192",
  "org.postgresql"  % "postgresql"      % "42.2.1",
  "org.hibernate" % "hibernate-core" % "5.4.9.Final",
  javaWs
)

libraryDependencies += "org.mockito" % "mockito-core" % "2.10.0" % "test"

herokuAppName in Compile := "jasperdiflorio"
herokuJdkVersion in Compile := "11"