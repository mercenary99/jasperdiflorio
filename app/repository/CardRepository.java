package repository;

import models.Card;

import play.db.jpa.JPAApi;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.concurrent.CompletableFuture.supplyAsync;

public class CardRepository {
    private final JPAApi jpaApi;

    @Inject
    public CardRepository(JPAApi jpaApi) {
        this.jpaApi = jpaApi;
    }

    public CompletionStage<Card> add(Card card) {
        return supplyAsync(() -> wrap(em -> insert(em, card)));
    }

    public CompletionStage<Card> find(Long id) {
        return supplyAsync(() -> wrap(em -> find(em, id)));
    }

    public CompletionStage<Boolean> remove(Integer id) {
        return supplyAsync(() -> wrap(em -> remove(em, id)));
    }

    public CompletionStage<Stream<Card>> list() {
        return supplyAsync(() -> wrap(this::list));
    }

    public CompletionStage<Stream<String>> category(){
        return supplyAsync(() -> wrap(this::category));
    }

    public CompletionStage<Stream<String>> topic(String category){
        return supplyAsync(() -> wrap(em -> topic(em, category)));
    }

    public CompletionStage<Stream<Card>> topicCards(String topic){
        return supplyAsync(() -> wrap(em -> topicCards(em, topic)));
    }

    private <T> T wrap(Function<EntityManager, T> function) {
        return jpaApi.withTransaction(function);
    }

    private Card insert(EntityManager em, Card card) {
        em.persist(card);
        return card;
    }

    private Card find(EntityManager em, Long id) {
        return em.find(Card.class, id);
    }

    private Boolean remove(EntityManager em, Integer id) {
        Card card = em.find(Card.class, id);
        if(null != card) {
            em.remove(card);
            return true;
        } else {
            return false;
        }
    }

    private Stream<Card> list(EntityManager em) {
        List<Card> cards = em.createQuery("select c from card c", Card.class).getResultList();
        return cards.stream();
    }

    private Stream<String> category(EntityManager em){
        List<String> category = em.createQuery("select distinct category from card c", String.class).getResultList();
        return category.stream();
    }

    private Stream<String> topic(EntityManager em, String category){
        String sqlString = "select distinct topic from card c where category=\'" + category + "\'";
        List<String> topic = em.createQuery(sqlString, String.class).getResultList();
        return topic.stream();
    }

    private Stream<Card> topicCards(EntityManager em, String topic){
        String sqlString ="select c from card c where topic=\'" + topic + "\'";
        List<Card> cards = em.createQuery(sqlString,Card.class).getResultList();
        return cards.stream();
    }
}
