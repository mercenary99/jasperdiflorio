package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Card;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.CardService;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Collectors;

public class CardController extends Controller {

    private final CardService cardService;

    @Inject
    public CardController(CardService cardService) {
        this.cardService = cardService;
    }

    /**
     * Request to get all cards.
     * @param q string
     * @return request to cardService to get all cards.
     */
    public CompletionStage<Result> cards(String q) {
        return cardService.get().thenApplyAsync(cardStream -> ok(Json.toJson(cardStream.collect(Collectors.toList()))));
    }

    /**
     * Request to get a card with given identifier.
     * @param id card identifier
     * @return request to cardService to get a specific card with given identifier.
     */
    public CompletionStage<Result> get(long id) {
        return cardService.get(id).thenApplyAsync(card-> ok(play.libs.Json.toJson(card)));
    }

    /**
     * Request to add the given card.
     * @return request to cardService to add the card.
     */
    public CompletionStage<Result> add(Http.Request request) {
        JsonNode jsonCard = request.body().asJson();
        Card newCard = play.libs.Json.fromJson(jsonCard, Card.class);
        return cardService.add(newCard).thenApplyAsync(card -> ok(play.libs.Json.toJson(card)));
    }

    /**
     * Request to remove a card with given identifier.
     * @param id card identifier
     * @return request to cardService to delete a specific card with given identifier.
     */
    public CompletionStage<Result> delete(Integer id) {
        return cardService.delete(id).thenApplyAsync(removed -> removed ? ok() : internalServerError());
    }

    /**
     * Request to get a list of all categorys.
     * @return request to cardService to get a list of all categories
     */
    public CompletionStage<Result> category(){
        return cardService.category().thenApplyAsync(category -> ok(play.libs.Json.toJson(category)));
    }

    /**
     * Request to get a list of all topics of a category.
     * @param string category identifier 
     * @return request to cardServie to get a list of all topics of one category
     */
    public CompletionStage<Result> topic(String category){
        return cardService.topic(category).thenApplyAsync(topic -> ok(play.libs.Json.toJson(topic)));
    }

    /**
     * Request to get a list of all cards of one topic.
     * @param string topic identifier
     * @return request to cardService to get a list of all cards of one topic
     */
    public CompletionStage<Result> topicCards(String topic){
        return cardService.topicCards(topic).thenApplyAsync(cards -> ok(Json.toJson(cards.collect(Collectors.toList()))));
    }
}
