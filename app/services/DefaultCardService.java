package services;

import models.Card;
import repository.CardRepository;

import javax.inject.Inject;
import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

public class DefaultCardService implements CardService{

    private CardRepository cardRepository;

    @Inject
    public DefaultCardService(CardRepository bookRepository) {
        this.cardRepository = bookRepository;
    }

    public CompletionStage<Stream<Card>> get() {
        return cardRepository.list();
    }

    public CompletionStage<Card> get(Long id) {
        return cardRepository.find(id);
    }

    public CompletionStage<Boolean> delete(Integer id) {
        return cardRepository.remove(id);
    }

    public CompletionStage<Card> add(Card card) {
        return cardRepository.add(card);
    }

    public CompletionStage<Stream<String>> category(){
        return cardRepository.category();
    }

    public CompletionStage<Stream<String>> topic(String category){
        return cardRepository.topic(category);
    }

    public CompletionStage<Stream<Card>> topicCards(String topic){
        return cardRepository.topicCards(topic);
    }

}