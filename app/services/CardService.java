package services;

import java.util.concurrent.CompletionStage;
import java.util.stream.Stream;

import com.google.inject.ImplementedBy;

import models.Card;

@ImplementedBy(DefaultCardService.class)

public interface CardService {

    /**
     * Return's list of all cards.
     * @return list of all cards
     */
    CompletionStage<Stream<Card>> get();

    /**
     * Returns cards with given identifier.
     * @param id card identifier
     * @return card with given identifier or {@code null}
     */
    CompletionStage<Card> get(final Long id);

    /**
     * Removes card with given identifier.
     * @param id card identifier
     * @return {@code true} on success  {@code false} on failure
     */
    CompletionStage<Boolean> delete(final Integer id);

    /**
     * Adds the given card.
     * @param card to add
     * @return added card
     */
    CompletionStage<Card> add(final Card card);

    /**
     * Return's list of all categorys.
     * @return list of all categorys
     */
    CompletionStage<Stream<String>> category();

    /**
     * Return's list of all topics.
     * @return list of all topics
     */
    CompletionStage<Stream<String>> topic(String category);

    /**
     * Return's list of all cards of one topic.
     * @param string topic identifier
     * @return list of all cards of one topic
     */
    CompletionStage<Stream<Card>> topicCards(String topic);

}
