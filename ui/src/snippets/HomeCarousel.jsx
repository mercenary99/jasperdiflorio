import React from 'react';
import Container from 'react-bootstrap/Container';
import Carousel from 'react-bootstrap/Carousel';
import CarouselItem from 'react-bootstrap/CarouselItem';
import Row from 'react-bootstrap/Row';
import KarteiCard from './KarteiCard.jsx';

class HomeCarousel extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            category: [],
            topics: []
        }
    }

    // Request to get all categories
    componentDidMount() {
        fetch('/api/category')
            .then((response => {
                return response.json();
            }))
            .then(cat => {
                var top = [];
                // Request to get all topics out of a category
                cat.forEach(i => {
                    fetch('/api/topic/category/' + i)
                        .then((response => {
                            return response.json();
                        }))
                        .then(data => {
                            top = top.concat(data);
                            this.setState({ topics: top })
                        });
                });
                this.setState({ category: cat });
            });
    }

    render() {
        return (
            <Container>
                <Carousel controls={false} touch='true' indicators={false} interval={2000} bg='dark' variant='dark'>
                    {
                        this.state.topics.map(
                            function(topic) {
                                return (
                                    <CarouselItem>
                                        <Row className='justify-content-center'>
                                            <KarteiCard topic={topic} />
                                        </Row>
                                    </CarouselItem>
                                )
                            }
                        )
                    }
                </Carousel>
            </Container>
        );
    }
}

export default HomeCarousel;
