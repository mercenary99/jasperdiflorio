import React from 'react';
import Card from 'react-bootstrap/Card';


class LearnCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            update: false,
        }
    }

    render() {
        return (
            <Card style={{ width: '18rem' , borderRadius: '2em'}} className="lernCard">
                <Card.Header style={{backgroundColor: 'transparent'}}>{this.props.card.topic} </Card.Header>
                <Card.Body className="lernCardBody">
                    <Card.Title>{this.props.card.front}</Card.Title>
                </Card.Body>
                <Card.Footer style={{backgroundColor: 'transparent'}}>
                    Karte {this.props.number} von {this.props.length}
                </Card.Footer>
            </Card>
        );
    }
}

export default LearnCard;