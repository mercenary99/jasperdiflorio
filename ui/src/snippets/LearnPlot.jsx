import React from 'react';
import { XYPlot, XAxis, VerticalBarSeries, LabelSeries } from 'react-vis';
import '../../node_modules/react-vis/dist/style.css';

class LearnPlot extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        var data = [{ color: 1, x: 'Richtig', y: this.props.richtig }, { color: 2, x: 'Falsch', y: this.props.falsch }]

        return (
            <XYPlot
                xType="ordinal"
                xDistance={100}
                width={300}
                height={300}
                colorType='category'
                colorRange={['green', 'red']}>
                <XAxis />
                <VerticalBarSeries data={data} colorRange={['#d9ffb0', '#ff1a1a']} />
                <LabelSeries data={data} getLabel={d => d.y} />
            </XYPlot>
        );
    }
}

export default LearnPlot;