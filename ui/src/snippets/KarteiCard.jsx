import React from 'react';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';

class KarteiCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }


    render() {
        var url = '/learn/' + this.props.topic;
        var newCard = '/newCard/' + this.props.category + '/' + this.props.topic;
        var overview = '/overview/' + this.props.category + '/' + this.props.topic;
        return (
            <div className='justify-content-center'>
                <Card style={{ width: '18rem' , borderRadius: '2em'}}>
                    <Card.Title className={'karteiCard'}>{this.props.topic}</Card.Title>
                    <div>
                        <Button className="karteiButton" as={Link} to={url} bg='dark' variant='dark'>
                            Lernen
                        </Button>
                    </div>
                    {
                        this.props.karteien ?
                            <div>
                                <Button className="karteiButton newCard" as={Link} to={newCard} bg='dark' variant='dark'>
                                    +
                                </Button>
                                <Button className="karteiButton" as={Link} to={overview} bg='dark' variant='dark'>
                                    Übersicht
                                </Button>
                            </div>
                            : null
                    }
                </Card>
            </div>
        );
    }
}

export default KarteiCard;