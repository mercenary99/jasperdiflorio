import React from 'react';
import Container from 'react-bootstrap/Container';
import CardGroup from 'react-bootstrap/CardGroup';
import KarteiCard from './KarteiCard';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

class Karteien extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            topics: []
        }
    }

    // Request to get all topics out of a category
    componentDidMount() {
        fetch('/api/topic/category/' + this.props.category)
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({ topics: data })
            });
    }

    render() {
        const pCategory = this.props.category;
        const newTopic = '/newTopic/' + pCategory;
        return (
            <Container>
                <div id='aroundCategory' className='flex'>
                <h2>{this.props.category}</h2>
                <Col className='justify-content-center'>
                    <CardGroup className='justify-content-center'>
                        {
                            this.state.topics.length > 0 ?
                                this.state.topics.map(function(top) {
                                    return (
                                        <KarteiCard topic={top} category={pCategory} karteien={true} />
                                    );
                                }) : null
                        }
                    </CardGroup>
                    <Button className={'newTopic'} as={Link} to={newTopic} bg='dark' variant='dark'>+</Button>
                </Col>
                </div>
            </Container>
        );
    }

}

export default Karteien;