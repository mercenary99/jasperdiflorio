import React from 'react';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

class AllCards extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.deleteCard = this.deleteCard.bind(this);
    }

    // Function clicking delet button
    deleteCard() {
        fetch('/api/cards/' + this.props.card.id, {
            method: 'DELETE'
        })
            .then(response => {
                      if(response.status === 200) {
                          console.log(response.status);
                      } else {
                          console.log(response);
                      }

                  }
            );
        // reloading the page
        var topic = this.props.card.topic;
        var category = this.props.card.category;
        var path = '/overview/' + category + '/' + topic;
        history.push(path);
        history.go();
    }

    render() {
        return (
            <div className='justify-content-center'>
                <Card style={{ width: '18rem' }}>
                    <Card.Title>Frage: {this.props.card.front}</Card.Title>
                    <Card.Title>Antwort: {this.props.card.back}</Card.Title>
                    <div>
                    <Button bg='dark' variant='dark' onClick={this.deleteCard}>Karte löschen</Button>
                    </div>
                </Card>
            </div>
        );
    }
}

export default AllCards;