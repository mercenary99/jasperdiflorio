import React from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';

class NewCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            topic: "",
            category: "",
            front: "",
            back: "",
            valid: false
        }
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateField = this.validateField.bind(this);
        this.validateAll = this.validateAll.bind(this);
        this.regexTest = this.regexTest.bind(this);
    }

    // Regextest for inputfields, used for speciall characters
    regexTest(field) {
        let re = /^[\u00C0-\u017Fa-zA-Z0-9\u002D]+/;
        if(re.test(field.value)) {
            field.classList.remove("is-invalid");
            field.classList.add("is-valid");
            return true;
        } else {
            field.classList.remove("is-valid");
            field.classList.add("is-invalid");
            return false;
        }
    }

    // Validation
    validateField(fieldName) {
        let field = document.getElementById(fieldName);
        if(field.value.length >= 1 && field.value.length <= 20) {
            if(this.regexTest(field)) {
                return true;
            } else {
                return false;
            }
        } else {
            field.classList.remove("is-valid");
            field.classList.add("is-invalid");
            return false;
        }
    }

    // Validation
    validateAll() {
        this.validateField("inputFrage");
        this.validateField("inputAntwort");

        if(this.validateField("inputFrage")
            && this.validateField("inputAntwort")) {
            this.setState({ valid: true });
            return true;
        } else {
            this.setState({ valid: false });
            return false;
        }
    }

    // Read the answer out of the input field
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.validateAll();
        this.setState(
            { [name]: value }
        );
    }

    // Function clicking submitbutton
    handleSubmit() {
        const card =
            {
                'topic': this.props.match.params.topic,
                'category': this.props.match.params.category,
                'front': this.state.front,
                'back': this.state.back
            }
        const jcard = JSON.stringify(card)
        fetch('/api/cards', {
            method: 'POST',
            body: jcard,
            headers: {
                'Content-Type': 'application/json'
            }
        })
    }

    render() { 
        return ( 
            <Container style={{width: '40vh'}}>
                <h2>Neue Karte zu {this.props.match.params.topic} hinzufügen</h2>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group controlId="Frage">
                        <Form.Label>Frage:</Form.Label>
                        <Form.Control type="text" name="front" id="inputFrage" value={this.state.front}
                            onChange={this.handleInputChange}
                        />
                        <Form.Control.Feedback type="valid">Sieht gut aus!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">Geben Sie bitte eine Frage ein!</Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group controlId="Antwort">
                        <Form.Label>Antwort:</Form.Label>
                        <Form.Control type="text" name="back" id="inputAntwort" value={this.state.back}
                            onChange={this.handleInputChange}
                        />
                        <Form.Control.Feedback type="valid">Sieht gut aus!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">Geben Sie bitte eine Antowrt ein!</Form.Control.Feedback>
                    </Form.Group>
                    <Button type="submit" disabled={!this.state.valid} bg='dark' variant='dark'>
                        Neue Karte hinzufügen
                    </Button>
                </Form>
            </Container>
        );
    }
}

export default NewCard;
