import React from 'react';

import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import {createBrowserHistory} from 'history';

const history = createBrowserHistory();

class NewKartei extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            valid: false
        };
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.validateField = this.validateField.bind(this);
        this.validateAll = this.validateAll.bind(this);
        this.regexTest = this.regexTest.bind(this);
    }

    // Regextest for inputfields, used for speciall characters
    regexTest(field) {
        let re = /^[\u00C0-\u017Fa-zA-Z0-9\u002D]+/;
        if (re.test(field.value)) {
            field.classList.remove("is-invalid");
            field.classList.add("is-valid");
            return true;
        } else {
            field.classList.remove("is-valid");
            field.classList.add("is-invalid");
            return false;
        }
    }

    // Validation
    validateField(fieldName) {
        let field = document.getElementById(fieldName);
        if (field.value.length >= 1 && field.value.length <= 20) {
            if (this.regexTest(field)) {
                return true;
            } else {
                return false;
            }
        } else {
            field.classList.remove("is-valid");
            field.classList.add("is-invalid");
            return false;
        }
    }

    // Validation
    validateAll() {
        if(this.validateField("inputTopic")) {
                this.setState({valid: true});
                return true;
        } else {
            this.setState({valid: false});
            return false;
        }
    }

    // Function clicking submitbutton
    handleSubmit(e) {
        e.preventDefault();
        var category = this.props.match.params.category;
        var topic = this.state.topic;
        const path = '/newCard/'+category+'/'+topic
        history.push(path);
        history.go();
    }

    // Read the answer out of the input field
    handleInputChange(event) {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        this.validateAll();
        this.setState({
            [name]: value
        });
    }

    render() {
        return (
            <Container style={{width: '50vh'}}>
                <h2>Neues Thema für Kategorie: {this.props.match.params.category}</h2>
                <Form onSubmit={this.handleSubmit}>
                    <Form.Group controlId="topic">
                        <Form.Label>
                            Thema:
                        </Form.Label>
                        <Form.Control type="text" name="topic" id="inputTopic" value={this.state.topic}
                                      onChange={this.handleInputChange}
                        />
                        <Form.Control.Feedback type="valid">Sieht gut aus!</Form.Control.Feedback>
                        <Form.Control.Feedback type="invalid">Geben Sie bitte ein Thema ein!</Form.Control.Feedback>
                    </Form.Group>
                    <Button type="submit" disabled={!this.state.valid} bg='dark' variant='dark'>
                        Neues Thema hinzufügen
                    </Button>

                </Form>
            </Container>
        );
    }
}

export default NewKartei;