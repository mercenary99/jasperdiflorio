import React from 'react';
import LearnCard from '../snippets/LearnCard.jsx';
import Button from 'react-bootstrap/Button'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Modal from 'react-bootstrap/Modal';
import LearnPlot from '../snippets/LearnPlot.jsx';
import { createBrowserHistory } from 'history';
import Col from 'react-bootstrap/Col';

const history = createBrowserHistory();

class Learn extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: [],
            number: 0,
            first: true,
            last: false,
            stupid: true,
            rf: 'Falsch',
            setShow: false,
            richtig: 0,
            falsch: 0,
            path: ''
        };
        this.onNext = this.onNext.bind(this);
        this.onStupid = this.onStupid.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleErgebnis = this.handleErgebnis.bind(this);
    }

    // Request to get a specific topic.
    componentDidMount() {

        fetch('/api/topic/' + this.props.match.params.topic)
            .then((response => {
                return response.json();
            }))
            .then(data => {
                if(data.length === 1) {
                    this.setState(
                        { last: true }
                    )
                }
                if(data.length ===0){
                    history.push("/");
                    history.go();
                }
                this.setState(
                    { cards: data }
                );
            });
    }

    // Function clicking Next Button.
    onNext() {
        var number = this.state.number + 1;
        this.setState({ number: number });
        this.setState({ setShow: false });
        this.state.rf === 'Falsch' ? // Checking if answer is rigth or false
            this.setState({ falsch: this.state.falsch + 1 })
            : this.setState({ richtig: this.state.richtig + 1 });
        this.setState({ stupid: true });
        this.setState({ rf: 'Falsch' })
        document.getElementById('antwortbox').value = '';
        number === this.state.cards.length - 1 ?
            this.setState({ last: true })
            : this.setState({ last: false });
    }

    // Function clicking the i'm stupid button
    onStupid() {
        document.getElementById('antwortbox').value = '';
        this.setState({ stupid: !this.state.stupid })
        this.setState({ setShow: true });
        this.setState({ rf: 'Falsch' })
    }

    // Function clicking check answers
    handleSubmit(e) {
        e.preventDefault();
        this.setState({ check: false });
        this.setState({ setShow: true });
    }

    // Function closing the answer window
    handleClose() {
        this.setState({ setShow: false })
        document.getElementById('antwortbox').value = '';
    }

    // Read the answer out of the input field
    handleChange(event) {
        if(event.target.value === this.state.cards[this.state.number].back) {
            this.setState({ rf: 'Richtig' });
        } else {
            this.setState({ rf: 'Falsch' });
        }
        this.setState({ loesung: event.target.value });
        this.setState({ check: true })

    }

    // Function show result
    handleErgebnis() {
        var richtig = this.state.richtig;
        var falsch = this.state.falsch;
        var topic = this.props.match.params.topic;
        this.state.rf === 'Falsch' ?
            falsch = falsch + 1
            : richtig = richtig + 1;
        var path = '/ergebnis/' + richtig + '/' + falsch + '/' + topic;
        history.push(path);
        history.go();
    }

    render() {
        var n = this.state.number;
        return (
            <Container>
                <Row className='justify-content-center'>
                    <Col>
                        <Row className='justify-content-center'>
                            {
                                this.state.cards.length > 0 ?
                                    <LearnCard key={this.state.cards[n].id}
                                        card={this.state.cards[n]}
                                        number={this.state.number + 1}
                                        length={this.state.cards.length} />
                                    : null
                            }
                        </Row>
                        <br />
                        <Row className='justify-content-center'>
                            <Form onSubmit={this.handleSubmit}>
                                <Form.Group>
                                    <Form.Control id='antwortbox' type='text' onChange={this.handleChange} placeholder="Antwort" />
                                    <br/>
                                    <Button type='submit' bg='dark' variant='dark'>
                                        Übeprüfen
                                    </Button>
                                </Form.Group>
                            </Form>
                        </Row>
                        <Row className='justify-content-center'>
                            <Button onClick={this.onStupid} variant='danger'>
                                Ich bin dumm zeig mir die Antwort
                            </Button>
                        </Row>
                    </Col>
                    <Col>
                        <Row className='justify-content-center'>
                            <LearnPlot richtig={this.state.richtig} falsch={this.state.falsch} />
                        </Row>
                    </Col>
                </Row>
                {
                    this.state.cards.length > 0 ?
                        <Modal show={this.state.setShow}
                            onHide={this.handleClose} animation={false} keyboard={false}
                            centered={true} size='sm' className={this.state.rf} backdrop='static'>
                            {
                                this.state.stupid ?
                                    <Modal.Header closeButton={false}>
                                        <Modal.Title>{this.state.rf}</Modal.Title>
                                    </Modal.Header>
                                    : null
                            }
                            {
                                !this.state.stupid ?
                                    <Modal.Body show={this.state.supid}><p className='solution'>Die Lösung
                                                                           wäre: {this.state.cards[this.state.number].back}</p>
                                    </Modal.Body>
                                    : null
                            }
                            <Modal.Footer>
                                <Button variant="secondary" onClick={this.handleClose} hidden={!this.state.stupid}>
                                    Noch einmal
                                </Button>
                                <Button bg='dark' variant='dark' onClick={this.onNext} hidden={this.state.last}>
                                    Nächste Karte
                                </Button>
                                <Button bg='dark' variant='dark' onClick={this.handleErgebnis} hidden={!this.state.last}>
                                    Ergebnis
                                </Button>
                            </Modal.Footer>
                        </Modal>
                        : null
                }

            </Container>
        );
    }
}

export default Learn;