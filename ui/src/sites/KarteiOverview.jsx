import React, { Component } from 'react';
import Karteien from "../snippets/Karteien";
import Button from "react-bootstrap/Button";
import { Link } from "react-router-dom";

class KarteiOverview extends Component {
    constructor(props) {
        super(props);
        this.state ={
            categories: []
        }
    }

    // Request to get all categories.
    componentDidMount() {
        fetch('/api/category')
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({ categories: data });
            });
    }

    render() {
        return (
            <div>
                <h1 id='titleCategories'>Übersicht über alle Kategorien</h1>
                <Button id='buttonNewCat' as={Link} to="/newKartei" bg='dark' variant='dark'>
                    Neue Kategorie
                </Button>
                {
                    this.state.categories.length > 0 ?  // Checking if one or many categories in the database
                        this.state.categories.map(function(category) {
                            return (<Karteien category={category} />);
                        }) : <h1>Fügen Sie iher erste Lernkartei hinzu.</h1>
                }
                <br/>
            </div>
        );
    }
}

export default KarteiOverview;
