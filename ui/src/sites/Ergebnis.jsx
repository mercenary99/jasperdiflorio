import React from 'react';
import LearnPlot from '../snippets/LearnPlot';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import { Link } from 'react-router-dom';

class Ergebnis extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        var again = '/learn/' + this.props.match.params.topic
        return (
            <Container>
                <Row className='justify-content-center'>
                    <h2>Dein Resultat</h2>
                </Row>
                <Row className='justify-content-center'>
                    <LearnPlot richtig={this.props.match.params.richtig} falsch={this.props.match.params.falsch} />
                </Row>
                <Row>
                    <Col>
                        <Button as={Link} to={again} bg='dark' variant='dark'>Noch einmal</Button>
                    </Col>
                    <Col>
                        <Button as={Link} to='/karteien' bg='dark' variant='dark'>Zurück zu Karteien</Button>
                    </Col>
                </Row>
            </Container>
        );
    }
}

export default Ergebnis;