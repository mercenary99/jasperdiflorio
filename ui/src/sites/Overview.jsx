import React from 'react';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import AllCards from '../snippets/AllCards.jsx';
import CardGroup from 'react-bootstrap/CardGroup';
import Col from 'react-bootstrap/Col';
import { Link } from 'react-router-dom';
import { createBrowserHistory } from 'history';

const history = createBrowserHistory();

class Overview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            cards: []
        };
        this.deleteAllCards = this.deleteAllCards.bind(this);
    }

        // Request to get all cards of a topic.
    componentDidMount() {
        fetch('/api/topic/' + this.props.match.params.topic)
            .then((response => {
                return response.json();
            }))
            .then(data => {
                var cards = [];
                var category = this.props.match.params.category;
                data.forEach(card => {
                    if (card.category === category) {
                        cards.push(card);
                    }
                });
                this.setState({
                    cards: cards
                });
                // change page to karteien
                if(data.length === 0) {
                    var path = '/karteien';
                    history.push(path);
                    history.go();
                }
            });
    }

    // Function clicking delet all button
    deleteAllCards() {
        this.state.cards.forEach(i => {
            fetch('/api/cards/' + i.id, {
                method: 'DELETE'
            })
                .then(response => {
                    if(response.status === 200) {
                        console.log(response.status);
                    } else {
                        console.log(response);
                    }
                });
        });
            // change page to karteien
            var path = '/karteien';
            history.push(path);
            history.go();
        console.log('button all clicked')
    }

    render() { 
        var newCard = '/newCard/' + this.props.match.params.category + '/' + this.props.match.params.topic;
        return (
            <Container>
                <h2>{this.props.match.params.topic}</h2>
                <Col className='justify-content-center'>
                    <CardGroup className='justify-content-center'>
                        {
                            this.state.cards.length > 0 ?
                                this.state.cards.map(function(card) {
                                    return (<AllCards key={card.id} card={card} />);
                                }) : null
                        }
                    </CardGroup>
                </Col>
                <Button onClick={this.deleteAllCards} bg='dark' variant='dark'>Alle Karten löschen</Button>
                <Button as={Link} to={newCard} bg='dark' variant='dark'>Neue Karte</Button>
            </Container>
        );
    }
}

export default Overview;
