import React from 'react';
import Container from 'react-bootstrap/Container';
import HomeCarousel from '../snippets/HomeCarousel';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    render() {
        return (
            <Container>
                <h1 className="titleHome">Willkommen bei Jasper di Florio</h1>
                <h4>Hier kannst du deine eigenen Lernkarteien erstellen und lernen.</h4>
                <h5>Viel spass beim lernen und viel Glück bei der nächsten Prüfung.</h5>
                <h3 className="titleOver">Deine Neusten Karten</h3>
                <HomeCarousel />
            </Container>
        );
    }
}

export default Home;