import React from 'react';
import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom'
class Top extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return (
         <Container>
                <Navbar bg='dark' variant='dark' fixed='top'>
                    <Navbar.Brand className='mr-auto'>
                      <Link to='/' className='hometext'>
                        Jasperdiflorio
                      </Link> 
                    </Navbar.Brand>
                      <Link to='/karteien'>
                        <Button variant='outline-light'>
                          Karteien 
                        </Button>
                      </Link> 
                </Navbar> 
            </Container> );
               }
}
 
export default Top;