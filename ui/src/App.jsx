// Import Package
import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
} from 'react-router-dom'
import Button from 'react-bootstrap/Button';

// Import sites 
import Learn from './sites/Learn.jsx';
import NewCard from './sites/NewCard.jsx'
import Home from './sites/Home.jsx';
import Bottom from './layout/Bottom.jsx'
import Top from './layout/Top.jsx';
import Ergebnis from './sites/Ergebnis.jsx';
import NewKartei from "./sites/NewKartei";
import Overview from './sites/Overview';
import KarteiOverview from './sites/KarteiOverview'
import NewTopic from './sites/NewTopic.jsx';

// Import .css
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';


class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            categories: []
        }
    }

    // Request to get all categories
    componentDidMount() {
        fetch('/api/category')
            .then((response => {
                return response.json();
            }))
            .then(data => {
                this.setState({ categories: data });
            });
    }

    render() {
        return (
            <div className="App">
                <Router>
                    <header className="App-header">
                        <Top />
                    </header>
                    <main>
                        <Switch>
                            <Route path="/karteien" component={KarteiOverview} />
                            <Route path="/learn/:topic" component={Learn} />
                            <Route path="/newCard/:category/:topic" component={NewCard} />
                            <Route path="/ergebnis/:richtig/:falsch/:topic" component={Ergebnis} />
                            <Route path="/overview/:category/:topic" component={Overview} />
                            <Route path="/newKartei" component={NewKartei} />
                            <Route path="/newTopic/:category" component={NewTopic} />
                            <Route exact path="/" component={Home} />
                            <Route component={NoMatchPage} />
                        </Switch>
                    </main>
                    <footer>
                        <Bottom />
                    </footer>
                </Router>
            </div>
        );
    }
}

const NoMatchPage = () => {
    return(
        <div>
            <h3>Diese Seite existiert nicht</h3>
            <p>Zurück zur Home Seite:</p>
            <Button as={Link} to="/" bg='dark' variant='dark'>
                Home
            </Button>
        </div>
    )
}

export default App;
