# Card schema

# --- !Ups
create table card(
id integer GENERATED ALWAYS AS IDENTITY,
topic varchar(50),
category varchar(50),
front varchar(50),
back varchar(50),
PRIMARY KEY (id)
);

# --- !Downs
drop table card;
